$().ready (function()
{
	$("#painel-Formulario").validate({

		rules:{
			nome: {
				required: true,
				minlength: 4
			},
			endereco: {
				required: true,
				minlength: 10
			},
			email: {
				required: true,
				email: true
		},
		agree: "required"
},
	
	messagens: {
		nome: {
			required: "Por Favor digite um nome",
			minlength:"O Nome deve possuir mais de 3 caracteres"
		},
		endereco:{
			required:"Por Favor digite um endereço",
			minlength: "O endereço deve possuir mais de 10 caracteres"
		},
		email: {
			required: "Por Favor digite u, e-mail",
			email: "Por Favor insira um e-mail válido"
		},
		agree: "Você tem de aceitar os termos de responsábilidade."
	    }
    });
});